# medimage-addon-ehr-medtech-evo-upgrader


## MedImage EHR Connector MedTech database inserts upgrader

This script is for MedImage installer use after MedTech make any database schema changes to their ATTACHMENTSMGR table in MedTech Evolution.  This has only happened once since 2017, in later 2023, with the upgrade release 6.3 (version 17) of MedTech Evolution, however it could also happen in future.

You can run the script by running C:\Medimage\addons\ehr-medtech-evolution\upgrade-database-inserts.bat in the Windows Filemanager on the Windows server that holds MedImage.

The script works by reading the last entered manual photo attachment, mimicing the data added, and then swapping over any fields with core MedImage-custom information e.g. the filename of the photo, it's name, it's size, and the author being "MedImage". The output SQL Insert statement is then copied over the configuration in config/ehrconnect.json, and future photos will be added with this template.

The script could still fail in future if 
a) MedTech completely change their database table
b) There are new data ID join fields which don't have any consistency between entries
	
But it should work in most cases where e.g. a new misc data field has been added.	
	
The second half of the functionality will ask the user whether they want to change historic MedImage photo entries, as well.
	
The output to this script is an interactive command-line message for the user.

## License
MIT license.


